
		<footer class="main col-xs-12">

			<nav id="footer">

				<?php wp_nav_menu(['menu'=>'footer']);?>

			</nav>

				<p class="copyright">Copyright &copy; 2015 by After School</p>
		</footer>

	</div><!-- /container -->
	<?php wp_footer();?>
	<script>

	    $(document).ready(function(){
	    	setMenu();
	    	$(window).resize(function(){
	    		setMenu();
	    	});
	    });

	    function setMenu() {
	    	if($(window).width() < 768) {
	    		$('nav#main').hide();
	    	} else {
	    		$('nav#main').show();
	    	}
	    }

		$('a.menu_toggle').click(function(){
			$('nav#main').toggle();
		});

	</script>
	
	</body>
</html>