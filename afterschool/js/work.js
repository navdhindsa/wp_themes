<script>

	    $(document).ready(function(){
	    	setMenu();
	    	$(window).resize(function(){
	    		setMenu();
	    	});
	    });

	    function setMenu() {
	    	if($(window).width() < 768) {
	    		$('nav#main').hide();
	    	} else {
	    		$('nav#main').show();
	    	}
	    }

		$('a.menu_toggle').click(function(){
			$('nav#main').toggle();
		});

	</script>
