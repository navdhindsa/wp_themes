<?php

if(function_exists('add_theme_support')){
	 add_theme_support('post-thumbnails');
	 add_theme_support('list-thumb',75,75);
}

//if the function does not exist
if(!function_exists('my_load_styles')){
	/**
	 * loads the stylesheet
	 * @param  String $content 
	 * @return action is added
	 */
	function my_load_styles($content)
	{
		wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', [], false, 'all' );
		
		wp_enqueue_style( 'my_classwork', get_stylesheet_uri(), ['bootstrap'], false, 'all' );

		wp_enqueue_script( 'bootstrapjs', 'https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', ['jquery'], false, true);
	}
	add_action('wp_enqueue_scripts','my_load_styles');
}