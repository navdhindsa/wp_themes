<?php 
/*
Theme Name: afterschool
 */
get_header(); ?>

		

		<div id="content" class="col-xs-12">


			<div id="primary" class="col-xs-12 col-sm-9">

				<h1 class="archive_title"><?=get_the_title(); ?></h1>


				<?php while(have_posts()) : the_post(); ?>
					 <article>

						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

									<?php the_date(); ?>
						<p><?php the_excerpt('<div>','</div>'); ?>[&hellip;]</p>

					</article>
				<?php endwhile; ?>

	 


	 

			</div><!-- /primary -->
			
			<?php get_sidebar(); ?>

		</div><!-- /content -->
	<?php get_footer(); ?>