<?php 
/*
Theme Name: afterschool
 */
get_header(); ?>

		

		<div id="content" class="col-xs-12">


			<div id="primary" class="col-xs-12 col-sm-9">

				<h1 class="archive_title"><?=get_the_title(); ?></h1>


				<?php while(have_posts()) : the_post(); ?>
					 <article>


									<?php the_date(); ?>
						<p><?php the_content('<div>','</div>'); ?></p>
						<?php comments_template(); ?>
					</article>
				<?php endwhile; ?>

	 

<?php the_posts_navigation('prev','next'); ?>
	 

			</div><!-- /primary -->
			
			<?php get_sidebar(); ?>

		</div><!-- /content -->
	<?php get_footer(); ?>