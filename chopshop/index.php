<?php get_header(); ?>
		<h1>The Blog</h1>
		<?php get_sidebar(); ?>
		<div class="main">
			<?php while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title('<h1>','</h1>'); ?></a>
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_excerpt('<div>','</div>'); ?>
				<a href="<?php the_permalink(); ?>">Read More</a>

				
			<?php endwhile; ?>
		</div>
	</div>
<?php get_footer(); ?>