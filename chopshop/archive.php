<?php 
//Template Name:archive
get_header(); ?>
		<?php get_sidebar(); ?>
		<div class="main">
			<h1><?php the_archive_title(); ?></h1>
			<?php while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title('<h3>','</h3>'); ?></a>
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_excerpt('<div>','</div>'); ?>

				<a href="<?php the_permalink(); ?>">Read More</a>

				
			<?php endwhile; ?>
		</div><!-- Main-->
		
	</div><!-- Container-->
	<?php get_footer(); ?>