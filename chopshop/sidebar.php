<div class="sidebar">
			<h2>Side Bar</h2>
			<h3>Search</h3>
			<form action="/" method="get">
				<input type="text" name="s">
				<input type="submit" value="Search">
			</form>
			<h3>Menu</h3>
			<?php wp_nav_menu();?>
			
			<h3>Archives</h3>
			<?php wp_get_archives(); ?>
			
			<h3>Categories</h3>
			<?php wp_list_categories('title_li'); ?>
			
		</div><!-- Side bar-->