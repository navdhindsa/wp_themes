<?php 
//Template Name:search
get_header(); ?>

		<?php get_sidebar('page'); ?>
		<div class="main">
			<h1><?= get_search_query(); ?></h1>
			<?php while(have_posts()) : the_post(); ?>
				<?php the_title('<h1>','</h1>'); ?>
				
				<?php the_content('<div>','</div>'); ?>
				
				
			<?php endwhile; ?>
		</div><!-- Main-->
		
	</div><!-- Container-->
	<?php get_footer(); ?>