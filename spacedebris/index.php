<?php 
/*
Theme Name: spacedebris
 */
 ?><!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
    <link rel="stylesheet" href="<?=get_stylesheet_uri(); ?>" >

    <title>Space Debris!</title>
    <?php wp_head(); ?>
  </head>
  <body>
  	<div class="container">
	    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<a class="navbar-brand" href="#">Navbar</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav">
					<a class="nav-item nav-link " href="#">Home</a>
					<a class="nav-item nav-link" href="#">Debris</a>
					<a class="nav-item nav-link" href="#">Tracking</a>
					<a class="nav-item nav-link" href="#">Supplies</a>
					<a class="nav-item nav-link" href="#">Join Us</a>
					<a class="nav-item nav-link" href="#">News</a>
				</div>
			</div>
		</nav>
		<div class="row">
			<div class="container col-md-12">
				<h1 class="center-align"> Bringing Space Debris Home!</h1>
			</div>
		</div>

		<div class="row">
			<div class="container col-md-12"><img class="slider_image" src= "<?=get_template_directory_uri(); ?>/images/slider.jpg" /></div> 	 
		    
		  
		</div>


	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <?php wp_footer();?>
  </body>
</html>